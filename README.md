﻿# GeneraliRSS

## A feladat
Feladat egy webes RSS olvasó alkalmazás készítése, melynek segítségével a felhasználó be tud
jelentkezni (egyszerűsített, megengedett a felhasználó és jelszó egyszerű tárolása), meg tudja adni az
általa követni kívánt RSS csatornákat. Az onnan letöltött híreket olvasottra tudja állítani. Opcionális
funkciók: hírek kedvencként való megjelölése, keresés hírek között, AJAX hívások alkalmazása, offline
működés támogatása.

Design Pattern: ASP.Net MVC
Adattárolás: SQL server
Fejlesztő eszköz: Visual Studio 201x

## Üzembehelyezés:
#### Adatbázis
  1. A leendő adatbázis nevét az `appsettings.json` fájlban lehet megadni a `ConnectionStrings` > `GeneraliRSSContext`-en belül lévő `Database` változóban
  2. Ha nem szeretné tesztadattal létrehozni az adatbázist, akkor a Migrations mappában található `..._InitialCreate.cs` nevű fájlban ki lehet kommentelni a sort ami meghívja az InsertTestData függvényt.
  3. Nyisson meg egy konzol ablakot a GeneraliRSS project mappában
  4. A `dotnet ef database update` parancs automatikusan felépíti az adatbázist

#### Elindítás
* A megoldást Visual Studio-val megnyitva fel kell, hogy ismerje a projectet. Így a start gombra (F5) kattintva el lehet indítani
* Debug módban indítva az alapértelmezett böngészőben megnyílik az oldal, de minden esetben a https://localhost:44339/ oldalon elérhető
* Az rss2json API haladó - de jelenleg ki nem használt - használatához kulcsra van szükség, amit az `rss_api_key` néven keres a program környezeti változók között.
Ezt a Project > GeneraliRSS Properties... menüpontban (Alt+P, P) a Debug fül alatt lehet beállítani.