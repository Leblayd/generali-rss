﻿using GeneraliRSS.Models;
using GeneraliRSS.Models.JoinTables;
using Microsoft.EntityFrameworkCore;

namespace GeneraliRSS.Data
{
    public class GeneraliRssContext : DbContext
    {
        public GeneraliRssContext(DbContextOptions<GeneraliRssContext> options)
            : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Source> Sources { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserSource>()
                .HasKey(us => new {us.UserName, us.SourceId});

            modelBuilder.Entity<UserSource>()
                .HasOne(pt => pt.User)
                .WithMany(p => p.UserSources)
                .HasForeignKey(pt => pt.UserName);

            modelBuilder.Entity<UserSource>()
                .HasOne(pt => pt.Source)
                .WithMany(t => t.UserSources)
                .HasForeignKey(pt => pt.SourceId);

            base.OnModelCreating(modelBuilder);
        }
    }
}