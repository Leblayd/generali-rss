﻿function OnFeedSelect() {
    const value = document.getElementById("new-feed-select").value;
    const feed = document.getElementById("custom-feed");
    const addButton = document.getElementById("add-to-feed");

    if (value === "custom") {
        feed.style.display = "inline-block";
        addButton.disabled = true;
    } else {
        feed.style.display = "none";
        addButton.disabled = false;
    }
}

function OnAddNewFeed() {
    const select = document.getElementById("new-feed-select");
    const option = document.createElement("option");
    option.text = document.getElementById("new-custom-feed").value;
    document.getElementById("new-custom-feed").value = "";
    select.add(option, select.options.length - 1);
    select.value = option.text;
    OnFeedSelect();
}

function OnAddToFeed() {
    const feed = document.getElementById("new-feed");
    if (!feed.value) return;

    $.ajax({
        url: "/api/Sources",
        method: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({
            Url: feed.value
        }),
        success: addToUser
    });

    function addToUser(response) {
        const username = document.getElementById("data-username").innerText;
        $.ajax({
            url: `/api/Users/${username}/Sources`,
            method: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(response),
            success: LoadFeed
        });
    }
}

function LoadFeed() {
    $.ajax({
        url: `/api/Users/${username}`,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            document.getElementById("news-container").innerHTML = "";
            response["userSources"].forEach((item) => 
                addNewsItem(item.source)
            );
        }
    });

    function addNewsItem(source) {
        const newsContainer = document.getElementById("news-container");

        $.ajax({
            // To get around CORS, call the api through our own site
            url: `/api/?site=${source.url}`,
            success: createDiv
        });

        function createDiv(response) {
            response.items.forEach(item => {
                const newsItem = document.createElement("div");
                const titleContainer = document.createElement("div");
                const title = document.createElement("a");
                // Category is currently not implemented
                //const category = document.createElement("div");
                const text = document.createElement("div");

                newsItem.className = "news-item";
                titleContainer.className = "news-item-title";
                text.className = "news-item-text";
                title.innerText = item.title;
                title.href = item.link;
                title.target = "_blank";
                text.innerHTML = item.description;
                //category.innerText = "";

                titleContainer.append(title);
                //titleContainer.append(category);
                newsItem.append(titleContainer);
                newsItem.append(text);
                newsContainer.append(newsItem);
            });
        }
    }
}

function getObjects(json, key) {
    var objects = [];
    for (var i in json) {
        if (!json.hasOwnProperty(i)) continue;
        if (typeof json[i] == 'object') {
            objects = objects.concat(getObjects(json[i], key));
        } else if (i == key) {
            objects.push(json);
        }
    }
    return objects;
}

$(document).ready(() => {
    username = document.getElementById("data-username").innerText;
});

let username;