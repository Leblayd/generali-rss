﻿using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GeneraliRSS.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Sources",
                columns: table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    Url = table.Column<string>()
                },
                constraints: table => table.PrimaryKey("PK_Sources", x => x.Id));

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Name = table.Column<string>(),
                    Password = table.Column<string>()
                },
                constraints: table => table.PrimaryKey("PK_Users", x => x.Name));

            migrationBuilder.CreateTable(
                name: "UserSource",
                columns: table => new
                {
                    UserName = table.Column<string>(),
                    SourceId = table.Column<int>()
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserSource", x => new {x.UserName, x.SourceId});
                    table.ForeignKey(
                        name: "FK_UserSource_Sources_SourceId",
                        column: x => x.SourceId,
                        principalTable: "Sources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserSource_Users_UserName",
                        column: x => x.UserName,
                        principalTable: "Users",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserSource_SourceId",
                table: "UserSource",
                column: "SourceId");

            InsertTestData(migrationBuilder);
        }

        private static void InsertTestData(MigrationBuilder migrationBuilder)
        {
            string[] usersCols = {"Name", "Password"};
            object[] usersData = {"admin", "admin"};
            migrationBuilder.InsertData("Users", usersCols, usersData);

            string sourcesCol = "Url";
            object[] sourcesData = {"https://www.engadget.com/rss-full.xml", "https://xkcd.com/rss.xml"};
            migrationBuilder.InsertData("Sources", sourcesCol, sourcesData);

            string[] userSourceCols = {"UserName", "SourceId"} ;
            object[,] userSourceData = {
                {"admin", "1"},
                {"admin", "2"}
            };
            migrationBuilder.InsertData("UserSource", userSourceCols, userSourceData);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserSource");

            migrationBuilder.DropTable(
                name: "Sources");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}