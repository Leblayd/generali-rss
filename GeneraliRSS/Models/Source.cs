﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GeneraliRSS.Models.JoinTables;
using Newtonsoft.Json;

namespace GeneraliRSS.Models
{
    public class Source
    {
        public int Id { get; set; }
        [Required] public string Url { get; set; }
        [JsonIgnore] public List<UserSource> UserSources { get; set; } = new List<UserSource>();
    }
}