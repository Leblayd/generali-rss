﻿using Newtonsoft.Json;

namespace GeneraliRSS.Models.JoinTables
{
    public class UserSource
    {
        [JsonIgnore] public User User { get; set; }
        [JsonIgnore] public string UserName { get; set; }
        public Source Source { get; set; }
        [JsonIgnore] public int SourceId { get; set; }
    }
}