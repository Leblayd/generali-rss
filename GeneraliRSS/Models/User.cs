﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GeneraliRSS.Models.JoinTables;

namespace GeneraliRSS.Models
{
    public class User
    {
        [Key] [Required] public string Name { get; set; }
        [Required] public string Password { get; set; } // TODO hash password
        public List<UserSource> UserSources { get; set; } = new List<UserSource>();
    }
}