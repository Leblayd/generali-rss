﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml.Linq;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace GeneraliRSS.Controllers
{
    [Route("api")]
    [ApiController]
    public class CorsWorkaroundController : ControllerBase
    {
        // GET: api?site=http...
        [HttpGet]
        public object GetUsers([FromQuery] string site)
        {
            string apiKey = Environment.GetEnvironmentVariable("rss_api_key");
            
            WebClient client = new WebClient();
            string url = "https://api.rss2json.com/v1/api.json?rss_url=" + site;
            if (apiKey != null)
                url += "&api_key=" + apiKey;

            // TODO do more than just return the json as it is
            var json = JsonConvert.DeserializeObject(client.DownloadString(url));
            return json;
        }
    }
}