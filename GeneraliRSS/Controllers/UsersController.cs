﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GeneraliRSS;
using GeneraliRSS.Data;
using GeneraliRSS.Models;
using GeneraliRSS.Models.JoinTables;

namespace GeneraliRSS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    // TODO secure the api, because currently its freely accessible to anyone, and reveals every user's details
    public class UsersController : ControllerBase
    {
        private readonly GeneraliRssContext _context;

        public UsersController(GeneraliRssContext context)
        {
            _context = context;
        }

        // GET: api/Users
        [HttpGet]
        public IEnumerable<User> GetUsers()
        {
            return _context.Users
                .Include(u => u.UserSources)
                .ThenInclude(us => us.Source);
        }

        // GET: api/Users/John
        [HttpGet("{name}")]
        public async Task<IActionResult> GetUser([FromRoute] string name)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            User user = await _context.Users
                .Include(u => u.UserSources)
                .ThenInclude(us => us.Source)
                .FirstAsync(s => s.Name == name);
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        // PUT: api/Users/John
        [HttpPut("{name}")]
        public async Task<IActionResult> PutUser([FromRoute] string name, [FromBody] User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (name != user.Name)
            {
                return BadRequest();
            }

            _context.Entry(user).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(name))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Users
        [HttpPost]
        public async Task<IActionResult> PostUser([FromBody] User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (await _context.Users.FindAsync(user.Name) != null)
            {
                return Conflict();
            }


            _context.Users.Add(user);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUser", new {name = user.Name}, user);
        }

        // POST: api/Users/John/UserSources
        [HttpPost("{name}/Sources")]
        public async Task<IActionResult> PostUserSources([FromRoute] string name, [FromBody] Source source)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            User dbUser = await _context.Users.FindAsync(name);
            dbUser.UserSources.Add(new UserSource
            {
                SourceId = source.Id
            });
            _context.Entry(dbUser).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(name))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // DELETE: api/Users/John
        [HttpDelete("{name}")]
        public async Task<IActionResult> DeleteUser([FromRoute] int name)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await _context.Users.FindAsync(name);
            if (user == null)
            {
                return NotFound();
            }

            _context.Users.Remove(user);
            await _context.SaveChangesAsync();

            return Ok(user);
        }

        private bool UserExists(string name)
        {
            return _context.Users.Any(e => e.Name == name);
        }
    }
}