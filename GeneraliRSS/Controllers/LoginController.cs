﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading.Tasks;
using GeneraliRSS.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Session;

namespace GeneraliRSS.Controllers
{
    public class LoginController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("username") != null)
                return RedirectToAction("Index", "Home");
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromForm] User user)
        {
            var response = await GetClient().PostAsJsonAsync("/api/users", user);
            if (response.IsSuccessStatusCode)
            {
                HttpContext.Session.SetString("username", user.Name);
            }
            
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromForm] User user)
        {
            var response = await GetClient().GetAsync($"/api/users/{user.Name}");
            if (!response.IsSuccessStatusCode) return RedirectToAction("Index");

            var userInDb = await response.Content.ReadAsAsync<User>();
            if (userInDb?.Password == user.Password)
            {
                HttpContext.Session.SetString("username", userInDb.Name);
                return RedirectToAction("Index", "Home");
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Logout()
        {
            HttpContext.Session.Remove("username");
            return RedirectToAction("Index");
        }

        private HttpClient GetClient()
        {
            HttpRequest req = HttpContext.Request;
            return new HttpClient
            {
                BaseAddress = new Uri(req.Scheme + "://" + req.Host.Value)
            };
        }
    }
}